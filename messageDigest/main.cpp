/***
compile and run (in linux):
g++ sha256.cpp main.cpp -o sha256_example && ./sha256_example
**/

#include <iostream>
#include <fstream>
#include <cmath>
#include <string>

#include "sha256.h"
#include "BigIntegerLibrary.hh"

int main(int argc, char *argv[])
{
    try {
    // Check the command line argument to determine which mode to run
    // the program in
    switch (*argv[1]) {
        // Sign document
        case ('s'): { 
            // Read in the file 
            // Process adapted from BlockIO.cpp
            std::string filename = argv[2];
            std::ifstream in_file(filename.c_str(), std::ios::binary);

            std::streampos begin, end;
            
            begin = in_file.tellg();
            in_file.seekg(0, std::ios::end);

            end = in_file.tellg();
            std::streampos size = end - begin;

            in_file.seekg(0, std::ios::beg);

            char * memblock = new char[size];
            in_file.read(memblock, size);

            memblock[size] = '\0';
            in_file.close();

            std::string input(memblock);

            // Hash the input string
            std::string hash_output = sha256(input);

            // Convert the sha hex output to base 10 and make it a
            // BigInteger
            BigUnsignedInABase hash(hash_output, 16);
            BigInteger hash_output_int(hash);
            
            // Sign the file with private key
            // TODO: Allow a * for the directory containing keys?
            in_file.open("../bigInt435/d_n.txt");
            std::string d, n;

            in_file >> d;
            in_file >> n;

            in_file.close();

            // Convert the string to BigInt
            BigUnsigned d_int = stringToBigUnsigned(d);
            BigUnsigned n_int = stringToBigUnsigned(n);

            BigUnsigned signed_file = modexp(hash_output_int, d_int, n_int);
            std::string signed_file_string = bigUnsignedToString(signed_file);

            // Sign the message and save the signiture to an output file
            // Generate the name of the signiture file by adding
            // .signiture to the passed file name
            std::string out_file_name = argv[2];
            out_file_name += ".signiture";

            std::ofstream out_file(out_file_name);
            out_file << signed_file_string << '\n';


            //out_file.open(out_file_name);
            //out_file << signed_file_string << '\n';
            //out_file.close();
        } break;

        // Verify document
        case('v'): {
            // Read in the file 
            // Process adapted from BlockIO.cpp
            std::string filename = argv[2];
            std::ifstream in_file(filename.c_str(), std::ios::binary);

            std::streampos begin, end;
            
            begin = in_file.tellg();
            in_file.seekg(0, std::ios::end);

            end = in_file.tellg();
            std::streampos size = end - begin;

            in_file.seekg(0, std::ios::beg);

            char * memblock = new char[size];
            in_file.read(memblock, size);

            memblock[size] = '\0';
            in_file.close();

            std::string input(memblock);

            // Hash the input string
            std::string hash_output = sha256(input);

            // Convert the sha hex output to base 10 and make it a
            // BigInteger
            BigUnsignedInABase hash(hash_output, 16);
            BigInteger hash_output_int(hash);
            std::string hash_string = bigIntegerToString(hash_output_int);


            // Read the signiture file and modexp
            // Process adapted from BlockIO.cpp
            //std::string signiture_filename = argv[3];
            //std::ifstream signiture_in_file(filename.c_str(), std::ios::binary);

            //std::streampos signiture_begin, signiture_end;
            //
            //begin = in_file.tellg();
            //in_file.seekg(0, std::ios::end);

            //end = in_file.tellg();
            //std::streampos signiture_size = end - begin;

            //in_file.seekg(0, std::ios::beg);

            //char * signiture_memblock = new char[size];
            //in_file.read(memblock, size);

            //memblock[size] = '\0';
            //in_file.close();

            std::ifstream signiture_file_in;
            signiture_file_in.open(argv[3]);

            std::string signiture;

            signiture_file_in >> signiture;

            signiture_file_in.close();

            BigInteger signiture_int = stringToBigInteger(signiture);

            // Read in the key
            in_file.open("../bigInt435/e_n.txt");

            std::string e, n;

            in_file >> e;
            in_file >> n;

            in_file.close();

            BigUnsigned e_int = stringToBigUnsigned(e);
            BigUnsigned n_int = stringToBigUnsigned(n);

            BigUnsigned message_encrypt = modexp(signiture_int, e_int, n_int);
            std::string signiture_string = bigUnsignedToString(message_encrypt);

            // Compare the two hash strings
            if (hash_string == signiture_string) {
                std::cout << "The compared files have the same hash!\n";
            }
            else {
                std::cout << "The compared files do not have the same hash.\n";
            }
        } break;

        default: {
            std::cout << "Invalid mode parameter, try using s for sign mode or v for verify mode.\n";
            std::cout << "Ending program.\n";
        } break;
    }
}

    catch(char const* msg) {
        std::cout << msg << '\n';
    }
    
    return 0;
}

