// You need to complete this program for a part of your first project.

// Standard libraries
#include <string>
#include <iostream>
#include <stdlib.h> 

// Additional libraries included for project
#include <cmath>
#include <fstream>

// `BigIntegerLibrary.hh' includes all of the library headers.
#include "BigIntegerLibrary.hh"

BigUnsigned generate_big_int();
bool is_prime(const BigUnsigned&);

int main() {
    try {
        std::ofstream out_file;

        // Generate two big prime numbers p and q
        BigUnsigned p = generate_big_int();
        BigUnsigned q = generate_big_int();
        
        // Write p and q to a file, each on their own line
        out_file.open("p_q.txt");

        out_file << p << '\n';
        out_file << q << '\n';

        out_file.close();

        // Find n and phi of n
        BigInteger n = p * q;
        BigUnsigned phi_of_n = (p - 1) * (q - 1);

        // Generate an e < phi_of_n && prime
        BigUnsigned e = rand();
        for (int i = 0; i < 6; ++i) {
            e = e * 10 + rand();
        }

        if (e % 2 == 0) {
            e += 1;
        }

        while (e < phi_of_n && gcd(e, phi_of_n) != 1) {
            e += 2;
        }

        // Use a k = 1 because it is a constant for the equation
        BigInteger k = 1;
        BigInteger d = 0;
        BigInteger g = 1;

        // Perform the extended euclidean algorithm to find d
        // k is just a constant that will satisfy the equation
        extendedEuclidean(phi_of_n, e, g, k, d);

        // Check that d > 0
        if (d < 0) {
            d += phi_of_n;
        }

        // Write the e_n key
        out_file.open("e_n.txt");
        out_file << e << '\n';
        out_file << n << '\n';
        out_file.close();

        // Write the d_n key
        out_file.open("d_n.txt");
        out_file << d << '\n';
        out_file << n << '\n';
        out_file.close();
    } 
    catch (char const* err) {
        std::cout << "The library threw an exception: " << err << '\n';
    }

	return 0;
}

BigUnsigned generate_big_int() {
    BigUnsigned n(1);

    // Creat number of sufficient size
    for (int i = 0; i < 300; ++i) {
        n = (n * 10) + rand();
    }

    // Check to make sure the number is odd
    if (n % 2 == 0) {
        n += 1;
    }

    // Use Fermat's test to check if the number is prime
    while (!is_prime(n)) {
        n += 2;
    }

    return n;
}

bool is_prime(const BigUnsigned &p) {
    // Fermat's Primality Test
    // Get a random base
    BigInteger a = rand();

    // For this application pass the test twice
    if (modexp(a, p-1, p) == 1) {
        if (modexp(a+rand(), p-1, p) == 1) {
            return true;
        }
    } 

    return false;
}
